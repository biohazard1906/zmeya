#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include "../h/map.h"
#include "../h/snakeBot.h"


SnakeBot::SnakeBot(int x, int y) :deadFlagBot(false), _x(x),_y(y)
{
    snakeBot = { {_x,_y},{_x +snake_size,_y},{_x + 2 * snake_size,_y},{_x + snake_size,_y} };
}

SnakeBot::~SnakeBot()
{

}

void SnakeBot::Init(const sf::Texture& texture)
{

}

void SnakeBot::Move()
{
    switch (dir)
    {
        case 0:
            snakeBot.push_front({ snakeBot.front().x,snakeBot.front().y - snake_size });
            break;
        case 2:
            snakeBot.push_front({ snakeBot.front().x,snakeBot.front().y + snake_size });
            break;
        case 3:
            snakeBot.push_front({ snakeBot.front().x - snake_size,snakeBot.front().y });
            break;
        case 1:
            snakeBot.push_front({ snakeBot.front().x + snake_size,snakeBot.front().y });
            break;
    }

    snakeBot.pop_back();
}

void SnakeBot::isIntersectsSnake(std::list<sSSegment> snake, int &count_dead)
{
    sf::Clock clock;
   // while (clock.getElapsedTime() < sf::milliseconds(3)) {}
    for (std::list<sSSegment>::iterator i = snake.begin(), j = snakeBot.begin(); i != snake.end(); i++)
           {
                if (j->x == i->x && j->y == i->y)
                {
                    deadFlagBot = true;
                    break;
                }
            }

    if (deadFlagBot) {
        for (std::list<sSSegment>::iterator i = snakeBot.begin(); i != --snakeBot.end(); i++){
            i->x = 50000;
        }
        deadFlagBot = false;
        ++count_dead;
    }
}


void SnakeBot::Grow(int &nfoodX , int &nfoodY, bool &newFoodPosFound)
{
        while (!newFoodPosFound) {
            nfoodX = (rand() % (nScreenWidth / snake_size)) * snake_size;
            nfoodY = (rand() % (nScreenHeight / snake_size)) * snake_size;
            while ((nfoodY < 10 || nfoodY + snake_size > nScreenHeight || nfoodX < 10 || nfoodX + snake_size + 15 > nScreenWidth)) {
                nfoodX = (rand() % (nScreenWidth / snake_size)) * snake_size;
                nfoodY = (rand() % (nScreenHeight / snake_size)) * snake_size;
            }
            if (snakeBot.front().x != nfoodX && snakeBot.front().y != nfoodY)
            {
                newFoodPosFound = true;
            }

            //if fruit spawn in tail
            for (auto s : snakeBot) {
                if (s.x != nfoodX && s.y != nfoodY && newFoodPosFound) {
                    newFoodPosFound = true;
                }
                else {
                    newFoodPosFound = false;
                }
            }
        }

        //yveluchivaem tyshky
        snakeBot.push_back({ snakeBot.back().x,snakeBot.back().y });
}

void SnakeBot::Mind(int foodX, int foodY)
{
    if (dir == 1 || dir == 3)
    {
        dir = snakeBot.front().y < foodY ? 2 : 0;
    }
    if ((dir == 0 || dir == 2) && (snakeBot.front().x != foodX))
    {
        dir = snakeBot.front().x < foodX ? 1 : 3;
    }


    Move();
}



void SnakeBot::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}