#include "../h/puaseGameMulti.h"
#include "../h/multiplayer.h"
#include "../h/mainMenu.h"

#include <SFML/Window/Event.hpp>

PauseGameMulti::PauseGameMulti(std::shared_ptr<Context> &context)
        : m_context(context), m_isRestartButtonPressed(false),
          m_isRestartButtonSelected(true), m_isExitButtonSelected(false),
          m_isExitButtonPressed(false), m_isExitInMainMenuButtonSelected(false),
          m_isExitInMainMenuButtonPressed(false), m_indexButtonMenu2(0), m_countButtonMenu2(3)
{
}

PauseGameMulti::~PauseGameMulti()
{
}

void PauseGameMulti::Init()
{if(m_isExitButtonPressed)
    {
        m_context->m_window->close();
    }
    // Title
    m_pauseTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_pauseTitle.setString("Paused");
    m_pauseTitle.setOrigin(m_pauseTitle.getLocalBounds().width / 2,
                           m_pauseTitle.getLocalBounds().height / 2);
    m_pauseTitle.setPosition(m_context->m_window->getSize().x / 2 - 60.f,
                             m_context->m_window->getSize().y / 2 - 115.f);
    m_pauseTitle.setCharacterSize(60);
    m_pauseTitle.setFillColor(sf::Color::Blue);


    // Restart Button
    m_restartButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_restartButton.setString("Restart");
    m_restartButton.setOrigin(m_restartButton.getLocalBounds().width / 2,
                              m_restartButton.getLocalBounds().height / 2);
    m_restartButton.setPosition(m_context->m_window->getSize().x / 2 - 15.f,
                                m_context->m_window->getSize().y / 2 - 25.f);
    m_restartButton.setCharacterSize(30);

    // Exit in main menu Button
    m_exitInMainMenuButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitInMainMenuButton.setString("Exit in main menu");
    m_exitInMainMenuButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                                     m_exitButton.getLocalBounds().height / 2);
    m_exitInMainMenuButton.setPosition(m_context->m_window->getSize().x / 2 - 165.f,
                                       m_context->m_window->getSize().y / 2);
    m_exitInMainMenuButton.setCharacterSize(30);

    // Exit Button
    m_exitButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitButton.setString("Exit");
    m_exitButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                           m_exitButton.getLocalBounds().height / 2);
    m_exitButton.setPosition(m_context->m_window->getSize().x / 2 - 15.f,
                             m_context->m_window->getSize().y / 2 + 50.f);
    m_exitButton.setCharacterSize(30);
}

void PauseGameMulti::ProcessInput()
{
    sf::Event event;
    while (m_context->m_window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_context->m_window->close();
        }
        else if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Escape:
                {
                    m_context->m_works->setCurrent();
                    break;
                }
                case sf::Keyboard::Up:
                {
                    if (m_indexButtonMenu2 > 0) --m_indexButtonMenu2;
                    if (m_indexButtonMenu2 == 0)
                    {
                        m_isRestartButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu2 == 1)
                    {
                        m_isRestartButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu2 == 2)
                    {
                        m_isRestartButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down:
                {
                    if (m_indexButtonMenu2 < m_countButtonMenu2 - 1) ++m_indexButtonMenu2;

                    if (m_indexButtonMenu2 == 0)
                    {
                        m_isRestartButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu2 == 1)
                    {
                        m_isRestartButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu2 == 2)
                    {
                        m_isRestartButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Return: {
                    m_isExitButtonPressed = false;
                    m_isRestartButtonPressed = false;
                    if (m_isRestartButtonSelected) {
                        m_isRestartButtonPressed = true;
                    } else if(m_isExitInMainMenuButtonSelected) {
                        m_isExitInMainMenuButtonPressed = true;
                    } else {
                        m_isExitButtonPressed = true;
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

void PauseGameMulti::Update(sf::Time deltaTime)
{
    if(m_isRestartButtonSelected)
    {
        m_restartButton.setFillColor(sf::Color::Cyan);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_exitButton.setFillColor(sf::Color::White);
    }
    else if (m_isExitButtonSelected)
    {
        m_exitButton.setFillColor(sf::Color::Red);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_restartButton.setFillColor(sf::Color::White);
    } else {
        m_exitButton.setFillColor(sf::Color::White);
        m_restartButton.setFillColor(sf::Color::White);
        m_exitInMainMenuButton.setFillColor(sf::Color::Magenta);
    }

    if(m_isRestartButtonPressed)
    {
        defoult("../data/score.json");
        m_context->m_works->add(std::make_unique<Multiplayer>(m_context), true);
    }
    else if(m_isExitButtonPressed)
    {
        m_context->m_window->close();
    } else if (m_isExitInMainMenuButtonPressed)
    {
        defoult("../data/score.json");
        m_context->m_window->setSize(sf::Vector2u(640,352));
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }
}

void PauseGameMulti::Draw()
{
    m_context->m_window->draw(m_pauseTitle);
    m_context->m_window->draw(m_restartButton);
    m_context->m_window->draw(m_exitInMainMenuButton);
    m_context->m_window->draw(m_exitButton);
    m_context->m_window->display();
}