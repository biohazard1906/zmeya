#include "../h/map.h"
#include "../h/snake.h"
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

Snake::Snake(int x, int y) :deadFlag(false), _x(x),_y(y)
{
    ydav = { {_x,_y},{_x +snake_size,_y},{_x + 2 * snake_size,_y},{_x + snake_size,_y} };
} 

Snake::~Snake()
{

}

void Snake::Init(const sf::Texture& texture)
{

}

void Snake::Move(int dir)
{
    switch (dir)
    {
    case 0:
        ydav.push_front({ ydav.front().x,ydav.front().y - snake_size });
        break;
    case 2:
        ydav.push_front({ ydav.front().x,ydav.front().y + snake_size });
        break;
    case 3:
        ydav.push_front({ ydav.front().x - snake_size,ydav.front().y });
        break;
    case 1:
        ydav.push_front({ ydav.front().x + snake_size,ydav.front().y });
        break;
    }

    ydav.pop_back();
}

void Snake::isIntersectsSnake(std::list<sSSegment> snake)
{

    sf::Clock clock;
    //while (clock.getElapsedTime() < sf::milliseconds(3)) {}
    for (std::list<sSSegment>::iterator i = snake.begin(), j = ydav.begin(); i != snake.end(); i++)
    {
        if (j->x == i->x && j->y == i->y)
        {
            deadFlag = true;
        }
    }
}


void Snake::Grow(int& nfoodX, int& nfoodY, bool& newFoodPosFound)
{
    while (!newFoodPosFound) {
        nfoodX = (rand() % (nScreenWidth / snake_size)) * snake_size;
        nfoodY = (rand() % (nScreenHeight / snake_size)) * snake_size;
        while ((nfoodY < 10 || nfoodY + snake_size > nScreenHeight || nfoodX < 10 || nfoodX + snake_size + 15 > nScreenWidth)) {
            nfoodX = (rand() % (nScreenWidth / snake_size)) * snake_size;
            nfoodY = (rand() % (nScreenHeight / snake_size)) * snake_size;
        }
        if (ydav.front().x != nfoodX && ydav.front().y != nfoodY)
        {
            newFoodPosFound = true;
        }

        //if fruit spawn in tail
        for (auto s : ydav) {
            if (s.x != nfoodX && s.y != nfoodY && newFoodPosFound) {
                newFoodPosFound = true;
            }
            else {
                newFoodPosFound = false;
            }
        }
    }

    //yveluchivaem tyshky
    ydav.push_back({ ydav.back().x,ydav.back().y });
}

void Snake::isIntersectsSelf()
{
    for (std::list<sSSegment>::iterator i = ydav.begin(); i != ydav.end(); i++)
    {
        if (i != ydav.begin() && i->x == ydav.front().x && i->y == ydav.front().y)
        {
            deadFlag = true;
            break;
        }
    }
}



void Snake::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}

void Snake::WallMode()
{
    if (ydav.front().x < 10 || ydav.front().x + snake_size + 15 > nScreenWidth)
    {
        deadFlag = true;
    }
    if (ydav.front().y < 10 || ydav.front().y + snake_size> nScreenHeight)
    {
        deadFlag = true;
    }
    if (ydav.front().x < 10 || ydav.front().x + snake_size + 15 > nScreenWidth)
    {
        deadFlag = true;
    }
    if (ydav.front().y < 10 || ydav.front().y + snake_size > nScreenHeight)
    {
        deadFlag = true;
    }
}

void Snake::TeleportateThrowWall()
{
    if (ydav.front().x < 0)
    {
        ydav.front().x = nScreenWidth - snake_size;
    }
    if (ydav.front().y < 0)
    {
        ydav.front().y = nScreenHeight - snake_size;
    }
    if (ydav.front().x + snake_size > nScreenWidth)
    {
        ydav.front().x = 0;
    }
    if (ydav.front().y + snake_size > nScreenHeight)
    {
        ydav.front().y = 0;
    }
}
