#include "../h/sceneAsset.h"

SceneAsset::SceneAsset()
{
}

SceneAsset::~SceneAsset()
{
}

void SceneAsset::addTexture(int id, const std::string &filePath, bool wantRepeated)
{
    auto texture = std::make_unique<sf::Texture>();

    if(texture->loadFromFile(filePath))
    {
        texture->setRepeated(wantRepeated);
        m_textures[id] = std::move(texture);
    }
}

void SceneAsset::addFont(int id, const std::string &filePath)
{
    auto font = std::make_unique<sf::Font>();

    if(font->loadFromFile(filePath))
    {
        m_fonts[id] = std::move(font);
    }
}

const sf::Texture &SceneAsset::getTexture(int id) const
{
    return *(m_textures.at(id).get());
}

const sf::Font &SceneAsset::getFont(int id) const
{
    return *(m_fonts.at(id).get());
}
