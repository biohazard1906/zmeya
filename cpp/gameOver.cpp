#include "../h/gameOver.h"
#include "../h/mainMenu.h"
#include "../h/gamePlay.h"

#include <SFML/Window/Event.hpp>

GameOver::GameOver(std::shared_ptr<Context>& context)
    : m_context(context), m_isRetryButtonSelected(true),
    m_isRetryButtonPressed(false), m_isExitButtonSelected(false),
    m_isExitButtonPressed(false), m_isExitInMainMenuButtonSelected(false),
    m_isExitInMainMenuButtonPressed(false), m_indexButtonMenu(0), m_countButtonMenu(3)
{
}
GameOver::~GameOver()
{
}

void GameOver::Init()
{
    m_context->m_assets->addTexture(BACK, "../assets/textures/backSet.jpg");
    m_backGO.setTexture(m_context->m_assets->getTexture(BACK));

    // Title
    m_gameOverTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_gameOverTitle.setString("Game Over");
    m_gameOverTitle.setFillColor(sf::Color::Red);

    m_gameOverTitle.setOrigin(m_gameOverTitle.getLocalBounds().width / 2,
        m_gameOverTitle.getLocalBounds().height / 2);
    m_gameOverTitle.setPosition(m_context->m_window->getSize().x / 2 - 110.f,
        m_context->m_window->getSize().y / 2 - 150.f);
    m_gameOverTitle.setCharacterSize(60);


    // Play Button
    m_retryButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_retryButton.setString("Retry");
    m_retryButton.setOrigin(m_retryButton.getLocalBounds().width / 2,
        m_retryButton.getLocalBounds().height / 2);
    m_retryButton.setPosition(m_context->m_window->getSize().x / 2,
        m_context->m_window->getSize().y / 2 - 25.f);
    m_retryButton.setCharacterSize(30);

    // Exit in main menu Button
    m_exitInMainMenuButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitInMainMenuButton.setString("Exit in main menu");
    m_exitInMainMenuButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                                     m_exitButton.getLocalBounds().height / 2);
    m_exitInMainMenuButton.setPosition(m_context->m_window->getSize().x / 2 - 135.f,
                                       m_context->m_window->getSize().y / 2);
    m_exitInMainMenuButton.setCharacterSize(30);

    // Exit Button
    m_exitButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitButton.setString("Exit");
    m_exitButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
        m_exitButton.getLocalBounds().height / 2);
    m_exitButton.setPosition(m_context->m_window->getSize().x / 2,
        m_context->m_window->getSize().y / 2 + 50.f);
    m_exitButton.setCharacterSize(30);
}

void GameOver::ProcessInput()
{
    sf::Event event;
    while (m_context->m_window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_context->m_window->close();
        }
        else if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Up:
                {
                    if (m_indexButtonMenu > 0) --m_indexButtonMenu;
                    if (m_indexButtonMenu == 0)
                    {
                        m_isRetryButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isRetryButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isRetryButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down:
                {
                    if (m_indexButtonMenu < m_countButtonMenu - 1) ++m_indexButtonMenu;

                    if (m_indexButtonMenu == 0)
                    {
                        m_isRetryButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isRetryButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isRetryButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Return: {
                    m_isExitButtonPressed = false;
                    m_isRetryButtonPressed = false;
                    m_isExitInMainMenuButtonPressed = false;
                    if (m_isRetryButtonSelected) {
                        m_isRetryButtonPressed = true;
                    } else if(m_isExitInMainMenuButtonSelected) {
                        m_isExitInMainMenuButtonPressed = true;
                    } else {
                        m_isExitButtonPressed = true;
                    }

                    break;
                }
            default:
            {
                break;
            }
            }
        }
    }
}

void GameOver::Update(sf::Time deltaTime)
{
    if(m_isRetryButtonSelected)
    {
        m_retryButton.setFillColor(sf::Color::Green);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_exitButton.setFillColor(sf::Color::White);
    }
    else if (m_isExitButtonSelected)
    {
        m_exitButton.setFillColor(sf::Color::Red);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_retryButton.setFillColor(sf::Color::White);
    } else {
        m_exitButton.setFillColor(sf::Color::White);
        m_retryButton.setFillColor(sf::Color::White);
        m_exitInMainMenuButton.setFillColor(sf::Color::Magenta);
    }

    if(m_isRetryButtonPressed)
    {
        m_context->m_works->add(std::make_unique<GamePlay>(m_context), true);
    }
    else if(m_isExitButtonPressed)
    {
        m_context->m_window->close();
    } else if (m_isExitInMainMenuButtonPressed)
    {
        m_context->m_window->setSize(sf::Vector2u(640,352));
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }
}

void GameOver::Draw()
{
    m_context->m_window->clear();
    m_context->m_window->draw(m_backGO);

    m_context->m_window->draw(m_gameOverTitle);
    m_context->m_window->draw(m_retryButton);
    m_context->m_window->draw(m_exitInMainMenuButton);
    m_context->m_window->draw(m_exitButton);
    m_context->m_window->display();
}
   