#include "../h/gameWithBots.h"
#include "../h/gameOverBots.h"
#include "../h/pauseGameBots.h"
#include "../h/score.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>


#include <list>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;

int nfoodY = 200;
int nfoodX = 100;
GameWithBots::GameWithBots(std::shared_ptr<Context>& context)
        : m_context(context), deadBots(0)

{
    srand(time(nullptr));
}
GameWithBots::~GameWithBots()
{
}

void GameWithBots::Init() {

}

void GameWithBots::ProcessInput() {
    sf::Clock clock;
    numRound.setFont(m_context->m_assets->getFont(MAIN_FONT));
    numRound.setString(to_string(scoreArr[1] + scoreArr[2] + 1));
    if(arr[1] == 0){
        python.snakeTexture.loadFromFile("../assets/textures/snake.png");
        scoreArr[4] = 0;
    }
    if(arr[1] == 1){
        python.snakeTexture.loadFromFile("../assets/textures/snakered.png");
        scoreArr[4] = 1;
    }
    if(arr[1] == 2){
        python.snakeTexture.loadFromFile("../assets/textures/snakepurple.png");
        scoreArr[4] = 2;
    }

    wall.loadFromFile("../assets/textures/wall1.jpg");
    bot.snakeBotTexture.loadFromFile("../assets/textures/snakeyellow.png");
    bot2.snakeBotTexture.loadFromFile("../assets/textures/snakeyellow.png");
    foodTexture.loadFromFile("../assets/textures/food.png");
    m_context->m_assets->addTexture(BACK, "../assets/textures/grasss.jpg");
    background.setTexture(m_context->m_assets->getTexture(BACK));

    sf::Event event;
    while (m_context->m_window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_context->m_window->close();
        }

            //ypravlenie ydava
        else if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Up:
                    snake_direction = 0;
                    break;
                case sf::Keyboard::Down:
                    snake_direction = 2;
                    break;
                case sf::Keyboard::Left:
                    snake_direction = 3;
                    break;
                case sf::Keyboard::Right:
                    snake_direction = 1;
                    break;
                    if (arr[5] == 0) {
                        case sf::Keyboard::W:
                            snake_direction = 0;
                        break;
                        case sf::Keyboard::S:
                            snake_direction = 2;
                        break;
                        case sf::Keyboard::A:
                            snake_direction = 3;
                        break;
                        case sf::Keyboard::D:
                            snake_direction = 1;
                        break;
                    }
                        case sf::Keyboard::Escape:
                            m_context->m_works->add(std::make_unique<PauseGameBots>(m_context));
                        break;
                default:
                    break;
            }
        }
    }
    while (clock.getElapsedTime() < sf::milliseconds(100)) {}
    int _x, _y;
    m_context->m_window->clear();
    m_context->m_window->draw(background);

    if(arr[2] == 2) {
        bot.Mind(nfoodX, nfoodY);
        python.Move(snake_direction);

        bool newFoodPosFound = false;

        if (python.ydav.front().x == nfoodX && python.ydav.front().y == nfoodY) {
            python.Grow(nfoodX, nfoodY, newFoodPosFound);
        }

        if (bot.snakeBot.front().x == nfoodX && bot.snakeBot.front().y == nfoodY) {
            bot.Grow(nfoodX, nfoodY, newFoodPosFound);
        }


        if (arr[0] == 0) {
            for (auto index = 600, jindex = 0; index != -15; index -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 600, jindex = 315; index != -15; index -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 0, jindex = 15; jindex != 330; jindex += 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 585, jindex = 315; jindex != 0; jindex -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }

            for (auto index = 0; index < border_wall.size(); ++index) {
                for (auto border: border_wall[index]) {
                    sf::Sprite part;
                    part.setPosition(border.x, border.y);
                    part.setTexture(wall);
                    m_context->m_window->draw(part);
                }

            }
            python.WallMode();
        } else {
            python.TeleportateThrowWall();
        }


        bot2.Mind(nfoodX, nfoodY);
        if (bot2.snakeBot.front().x == nfoodX && bot2.snakeBot.front().y == nfoodY) {
            bot2.Grow(nfoodX, nfoodY, newFoodPosFound);
        }


        sf::Sprite food_shape;
        food_shape.setPosition(nfoodX, nfoodY);
        food_shape.setTexture(foodTexture);
        for (auto s1: python.ydav) {
            sf::Sprite part;
            part.setPosition(s1.x, s1.y);
            part.setTexture(python.snakeTexture);
            m_context->m_window->draw(part);
        }

        for (auto sb: bot.snakeBot) {
            sf::Sprite part;
            part.setPosition(sb.x, sb.y);
            part.setTexture(bot.snakeBotTexture);
            m_context->m_window->draw(part);
        }

        for (auto sb2: bot2.snakeBot) {
            sf::Sprite part;
            part.setPosition(sb2.x, sb2.y);
            part.setTexture(bot2.snakeBotTexture);
            m_context->m_window->draw(part);
        }

        m_context->m_window->draw(food_shape);
        m_context->m_window->draw(numRound);
        m_context->m_window->display();

        // while (clock.getElapsedTime() < sf::milliseconds(10)) {}
        python.isIntersectsSelf();
        python.isIntersectsSnake(bot.snakeBot);

        python.isIntersectsSnake(bot2.snakeBot);
        bot2.isIntersectsSnake(python.ydav, deadBots);
        bot.isIntersectsSnake(python.ydav, deadBots);
        bot2.isIntersectsSnake(bot.snakeBot, deadBots);
        bot.isIntersectsSnake(bot2.snakeBot, deadBots);

        if (python.deadFlag) {
            python.deadFlag = false;
            bot.deadFlagBot = false;
            bot2.deadFlagBot = false;
            if (scoreArr[2] == arr[3] / 2  || scoreArr[1] == arr[3] / 2) {
                defoult("../data/score.json");
                m_context->m_works->add(std::make_unique<GameOverBots>(m_context), true);
            } else {
                scoreArr[0] = 0;
                scoreArr[2] += 1;
                scoreArr[3] = 1;
                writeToFile(scoreArr, "../data/score.json");
                m_context->m_works->add(std::make_unique<Score>(m_context), true);
            }
        }
        if (deadBots == 2) {
            bot.deadFlagBot = false;
            bot2.deadFlagBot = false;
            if (scoreArr[2] == arr[3] / 2  || scoreArr[1] == arr[3] / 2) {
                defoult("../data/score.json");
                m_context->m_works->add(std::make_unique<GameOverBots>(m_context), true);
            } else {
                scoreArr[0] = 1;
                scoreArr[1] += 1;
                scoreArr[3] = 1;
                writeToFile(scoreArr, "../data/score.json");
                m_context->m_works->add(std::make_unique<Score>(m_context));
            }
        }
    } else {
        bot.Mind(nfoodX, nfoodY);
        python.Move(snake_direction);

        bool newFoodPosFound = false;

        if (python.ydav.front().x == nfoodX && python.ydav.front().y == nfoodY) {
            python.Grow(nfoodX, nfoodY, newFoodPosFound);
        }

        if (bot.snakeBot.front().x == nfoodX && bot.snakeBot.front().y == nfoodY) {
            bot.Grow(nfoodX, nfoodY, newFoodPosFound);
        }


        if (arr[0] == 0) {
            for (auto index = 600, jindex = 0; index != -15; index -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 600, jindex = 315; index != -15; index -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 0, jindex = 15; jindex != 330; jindex += 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }
            for (auto index = 585, jindex = 315; jindex != 0; jindex -= 15) {
                border_wall.push_back(std::list<sSSegment>{{index, jindex}});
            }

            for (auto index = 0; index < border_wall.size(); ++index) {
                for (auto border: border_wall[index]) {
                    sf::Sprite part;
                    part.setPosition(border.x, border.y);
                    part.setTexture(wall);
                    m_context->m_window->draw(part);
                }

            }
            python.WallMode();
        } else {
            python.TeleportateThrowWall();
        }




        sf::Sprite food_shape;
        food_shape.setPosition(nfoodX, nfoodY);
        food_shape.setTexture(foodTexture);
        for (auto s1: python.ydav) {
            sf::Sprite part;
            part.setPosition(s1.x, s1.y);
            part.setTexture(python.snakeTexture);
            m_context->m_window->draw(part);
        }

        for (auto sb: bot.snakeBot) {
            sf::Sprite part;
            part.setPosition(sb.x, sb.y);
            part.setTexture(bot.snakeBotTexture);
            m_context->m_window->draw(part);
        }


        m_context->m_window->draw(food_shape);
        m_context->m_window->draw(numRound);
        m_context->m_window->display();

        // while (clock.getElapsedTime() < sf::milliseconds(10)) {}
        python.isIntersectsSelf();
        python.isIntersectsSnake(bot.snakeBot);
        bot.isIntersectsSnake(python.ydav, deadBots);

        if (python.deadFlag) {
            python.deadFlag = false;
            bot.deadFlagBot = false;
            if (scoreArr[2] == arr[3] / 2  || scoreArr[1] == arr[3] / 2) {
                defoult("../data/score.json");
                m_context->m_works->add(std::make_unique<GameOverBots>(m_context), true);
            } else {
                scoreArr[0] = 0;
                scoreArr[2] += 1;
                scoreArr[3] = 1;
                writeToFile(scoreArr, "../data/score.json");
                m_context->m_works->add(std::make_unique<Score>(m_context), true);
            }
        }

            if (deadBots == 1) {
            bot.deadFlagBot = false;
            if (scoreArr[2] == arr[3] / 2 + 1 || scoreArr[1] == arr[3] / 2 + 1) {
                defoult("../data/score.json");
                m_context->m_works->add(std::make_unique<GameOverBots>(m_context), true);
            } else {
                scoreArr[0] = 1;
                scoreArr[1] += 1;
                scoreArr[3] = 1;
                writeToFile(scoreArr, "../data/score.json");
                m_context->m_works->add(std::make_unique<Score>(m_context), true);
                }
            }
    }


}

void GameWithBots::Update(sf::Time deltaTime) {
}



void GameWithBots::Draw() {

}

void GameWithBots::Pause() {

}

void GameWithBots::Start() {

}

