#include <SFML/Graphics/CircleShape.hpp>

#include "../h/game.h"
#include "../h/mainMenu.h"
#include "../h/loadToFromFile.h"

Game::Game() : m_context(std::make_shared<Context>())
{
    m_context->m_window->create(sf::VideoMode(600, 330), "Snake Game", sf::Style::Close);
    m_context->m_works->add(std::make_unique<MainMenu>(m_context));
}

Game::~Game()
{
}

void Game::Run()
{
    defoult("../data/settings.json");
    defoult("../data/score.json");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    sf::Clock clock;
    sf::Time timeSinceLastFrame = sf::Time::Zero;

    while (m_context->m_window->isOpen())
    {
        timeSinceLastFrame += clock.restart();

        while (timeSinceLastFrame > TIME_PER_FRAME)
        {
            timeSinceLastFrame -= TIME_PER_FRAME;

            m_context->m_works->processSceneChange();
            m_context->m_works->getCurrent()->ProcessInput();
            m_context->m_works->getCurrent()->Update(TIME_PER_FRAME);
            m_context->m_works->getCurrent()->Draw();
        }
    }
}

