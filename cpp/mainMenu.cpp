#include "../h/mainMenu.h"
#include "../h/multiplayer.h"
#include "../h/settings.h"
#include "../h/gameMod.h"


#include <SFML/Window/Event.hpp>

MainMenu::MainMenu(std::shared_ptr<Context> &context)
        : m_context(context), m_isPlayButtonSelected(true),
          m_isPlayButtonPressed(false), m_isExitButtonSelected(false),
          m_isExitButtonPressed(false), m_isSettingButtonSelected(false),
          m_isSettingButtonPressed(false), m_indexButtonMenu(0), m_countButtonMenu(3)
{
}

MainMenu::~MainMenu()
{
}

void MainMenu::Init()
{
    m_context->m_assets->addFont(MAIN_FONT, "../assets/fonts/MICKEY.TTF");
    m_context->m_assets->addTexture(BACK, "../assets/textures/back1.jpg");
    m_background.setTexture(m_context->m_assets->getTexture(BACK));


    // Title
    m_gameTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_gameTitle.setString("snake Game");
    m_gameTitle.setFillColor(sf::Color::Red);

    m_gameTitle.setOrigin(m_gameTitle.getLocalBounds().width / 2,
                          m_gameTitle.getLocalBounds().height / 2);
    m_gameTitle.setPosition(m_context->m_window->getSize().x / 2 - 110.f,
                            m_context->m_window->getSize().y / 2 - 150.f);
    m_gameTitle.setCharacterSize(60);


    // Play Button
    m_playButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_playButton.setString("Play");
    m_playButton.setOrigin(m_playButton.getLocalBounds().width / 2,
                           m_playButton.getLocalBounds().height / 2);
    m_playButton.setPosition(m_context->m_window->getSize().x / 2,
                             m_context->m_window->getSize().y / 2 - 25.f);
    m_playButton.setCharacterSize(30);


    //Setting button
    m_settingButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_settingButton.setString("Setting");
    m_settingButton.setOrigin(m_settingButton.getLocalBounds().width / 2,
                           m_settingButton.getLocalBounds().height / 2);
    m_settingButton.setPosition(m_context->m_window->getSize().x / 2,
                             m_context->m_window->getSize().y / 2);
    m_settingButton.setCharacterSize(30);

    // Exit Button
    m_exitButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitButton.setString("Exit");
    m_exitButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                           m_exitButton.getLocalBounds().height / 2);
    m_exitButton.setPosition(m_context->m_window->getSize().x / 2,
                             m_context->m_window->getSize().y / 2 + 25.f);
    m_exitButton.setCharacterSize(30);

}

void MainMenu::ProcessInput()
{

    sf::Event event;
    while (m_context->m_window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_context->m_window->close();
        }
        else if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Up:
                {
                    if(m_indexButtonMenu > 0) --m_indexButtonMenu;
                    if (m_indexButtonMenu == 0)
                    {
                        m_isPlayButtonSelected = true;
                        m_isSettingButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isPlayButtonSelected = false;
                        m_isSettingButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isPlayButtonSelected = false;
                        m_isSettingButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down:
                {
                    if(m_indexButtonMenu < m_countButtonMenu - 1) ++m_indexButtonMenu;

                    if (m_indexButtonMenu == 0)
                    {
                        m_isPlayButtonSelected = true;
                        m_isSettingButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isPlayButtonSelected = false;
                        m_isSettingButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isPlayButtonSelected = false;
                        m_isSettingButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Return:
                {
                    m_isPlayButtonPressed = false;
                    m_isExitButtonPressed = false;
                    m_isSettingButtonPressed = false;

                    if (m_isPlayButtonSelected) {
                        m_isPlayButtonPressed = true;
                    } else if(m_isSettingButtonSelected) {
                        m_isSettingButtonPressed = true;
                    } else {
                        m_isExitButtonPressed = true;
                    }
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

void MainMenu::Update(sf::Time deltaTime)
{
    if(m_isPlayButtonSelected)
    {
        m_playButton.setFillColor(sf::Color::Green);
        m_settingButton.setFillColor(sf::Color::White);
        m_exitButton.setFillColor(sf::Color::White);
    }
    else if (m_isExitButtonSelected)
    {
        m_exitButton.setFillColor(sf::Color::Red);
        m_settingButton.setFillColor(sf::Color::White);
        m_playButton.setFillColor(sf::Color::White);
    } else {
        m_exitButton.setFillColor(sf::Color::White);
        m_playButton.setFillColor(sf::Color::White);
        m_settingButton.setFillColor(sf::Color::Magenta);
    }

    if(m_isPlayButtonPressed)
    {
        m_context->m_works->add(std::make_unique<GameMode>(m_context), true);
    }
    else if(m_isExitButtonPressed)
    {
        m_context->m_window->close();
    }

    if (m_isSettingButtonPressed)
    {
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);

    }

}

void MainMenu::Draw()
{
    m_context->m_window->clear();
    m_context->m_window->draw(m_background);
    m_context->m_window->draw(m_gameTitle);
    m_context->m_window->draw(m_playButton);
    m_context->m_window->draw(m_settingButton);
    m_context->m_window->draw(m_exitButton);
    m_context->m_window->display();
}

