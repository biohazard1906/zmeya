#include "../h/gamePlay.h"
#include "../h/gameOver.h"
#include "../h/pauseGame.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>


#include <list>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;


GamePlay::GamePlay(std::shared_ptr<Context>& context)
        : m_context(context)

{
    srand(time(nullptr));
}
GamePlay::~GamePlay()
{
}

void GamePlay::Init() {

}

void GamePlay::ProcessInput() {
    sf::Clock clock;
    if(arr[1] == 0){
        python.snakeTexture.loadFromFile("../assets/textures/snake.png");
    }
    if(arr[1] == 1){
        python.snakeTexture.loadFromFile("../assets/textures/snakered.png");
    }
    if(arr[1] == 2){
        python.snakeTexture.loadFromFile("../assets/textures/snakepurple.png");
    }

    wall.loadFromFile("../assets/textures/wall1.jpg");
    foodTexture.loadFromFile("../assets/textures/food.png");
    m_context->m_assets->addTexture(BACK, "../assets/textures/grasss.jpg");
    background.setTexture(m_context->m_assets->getTexture(BACK));

    sf::Event event;
    while (m_context->m_window->pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_context->m_window->close();
        }

            //ypravlenie ydava
        else if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Up:
                    snake_direction = 0;
                    break;
                case sf::Keyboard::Down:
                    snake_direction = 2;
                    break;
                case sf::Keyboard::Left:
                    snake_direction = 3;
                    break;
                case sf::Keyboard::Right:
                    snake_direction = 1;
                    break;
                if (arr[5] == 0) {
                    case sf::Keyboard::W:
                        snake_direction = 0;
                    break;
                    case sf::Keyboard::S:
                        snake_direction = 2;
                    break;
                    case sf::Keyboard::A:
                        snake_direction = 3;
                    break;
                    case sf::Keyboard::D:
                        snake_direction = 1;
                    break;
                    case sf::Keyboard::Escape:
                        m_context->m_works->add(std::make_unique<PauseGame>(m_context));
                    break;
                }
                default:
                    break;
            }
        }
    }
    while (clock.getElapsedTime() < sf::milliseconds(100)) {}
    int _x, _y;
    m_context->m_window->clear();
    m_context->m_window->draw(background);

    python.Move(snake_direction);

    bool newFoodPosFound = false;

    if (python.ydav.front().x == nfoodX && python.ydav.front().y == nfoodY) {
        python.Grow(nfoodX, nfoodY, newFoodPosFound);
    }


    if (arr[0] == 0) {
        for (auto index = 600, jindex = 0; index != -15; index -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 600, jindex = 315; index != -15; index -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 0, jindex = 15; jindex != 330; jindex += 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 585, jindex = 315; jindex != 0; jindex -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});

        for (auto index = 0; index < border_wall.size(); ++index) {
            for (auto border: border_wall[index]) {
                sf::Sprite part;
                part.setPosition(border.x, border.y);
                part.setTexture(wall);
                m_context->m_window->draw(part);
            }

        }
        python.WallMode();
    } else {
    python.TeleportateThrowWall();
    }

    for (auto s1 : python.ydav)
    {

        sf::Sprite part;
        part.setPosition(s1.x, s1.y);
        part.setTexture(python.snakeTexture);
        m_context->m_window->draw(part);
    }

    sf::Sprite food_shape;
    food_shape.setPosition(nfoodX, nfoodY);
    food_shape.setTexture(foodTexture);
    m_context->m_window->draw(food_shape);
    python.isIntersectsSelf();

    m_context->m_window->display();
    if (python.deadFlag) {
        python.deadFlag = false;
        m_context->m_works->add(std::make_unique<GameOver>(m_context), true);
    }
}

void GamePlay::Update(sf::Time deltaTime) {
}



void GamePlay::Draw() {

}

void GamePlay::Pause() {

}

void GamePlay::Start() {

}

