#include "../h/mainMenu.h"
#include "../h/settings.h"
#include "../h/loadToFromFile.h"

#include <SFML/Window/Event.hpp>

Settings::Settings(std::shared_ptr<Context>& context)
        : m_context(context), m_isWithWallsSelected(true),
          m_isWithWallsPressed(false),
          m_isBotNumSelected(false),
          m_isBotNumPressed(false),
          m_isRoundNumSelected(false),
          m_isRoundNumPressed(false),
          m_isLayoutSelected(false),
          m_isLayoutPressed(false),
          m_isColorSelected(false), m_isColorPressed(false), m_exitInMainMenuButtonSelected(false),
          m_exitInMainMenuButtonPressed(false), m_indexButtonMenu1(0), m_countButtonMenu1(6)
{
}
Settings::~Settings()
{
}

std::string wallButton = "Classical mode(with walls)";
std::string color = "Blue";
std::string botButton = "1 bot";
std::string roundButton = "1 round";
std::string layout = "Arrows";

bool setCurrent = false;

int array[6];

void Settings::Init() {
    m_context->m_assets->addTexture(BACK, "../assets/textures/backSet.jpg");
    m_backGround.setTexture(m_context->m_assets->getTexture(BACK));

    // Title
    m_settingsTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_settingsTitle.setString("Settings");
    m_settingsTitle.setFillColor(sf::Color::Red);

    m_settingsTitle.setOrigin(m_settingsTitle.getLocalBounds().width / 2,
                              m_settingsTitle.getLocalBounds().height / 2);
    m_settingsTitle.setPosition(m_context->m_window->getSize().x / 2 - 60.f,
                                m_context->m_window->getSize().y / 2 - 150.f);
    m_settingsTitle.setCharacterSize(50);


    // with walls Button

    m_withWalls.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_withWalls.setString(wallButton);
    if (wallButton == "Classical mode(with walls)"){
    m_withWalls.setOrigin(m_withWalls.getLocalBounds().width / 2,
                          m_withWalls.getLocalBounds().height / 2);

        m_withWalls.setPosition(m_context->m_window->getSize().x / 2 + 80.f,
                                m_context->m_window->getSize().y / 2 - 25.f);
    }
    else{
        m_withWalls.setOrigin(m_withWalls.getLocalBounds().width / 2,
                                 m_withWalls.getLocalBounds().height / 2);
        m_withWalls.setPosition(m_context->m_window->getSize().x / 2 + 50.f,
                                   m_context->m_window->getSize().y / 2 - 25.f);
    }

    m_withWalls.setCharacterSize(20);
    m_withWalls.setFillColor(sf::Color::Black);


    //Color button
    m_color.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_color.setString(color);
    m_color.setOrigin(m_color.getLocalBounds().width / 2,
                      m_color.getLocalBounds().height / 2);
    m_color.setPosition(m_context->m_window->getSize().x / 2 + 10.f,
                       m_context->m_window->getSize().y / 2 + 25.f);
    m_color.setCharacterSize(20);
    m_color.setFillColor(sf::Color::Black);

    //Number of bots
    m_botNum.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_botNum.setString(botButton);
    m_botNum.setOrigin(m_botNum.getLocalBounds().width / 2,
                       m_botNum.getLocalBounds().height / 2);
    m_botNum.setPosition(m_context->m_window->getSize().x / 2 + 20.f,
                         m_context->m_window->getSize().y / 2 + 50.f);
    m_botNum.setCharacterSize(20);
    m_botNum.setFillColor(sf::Color::Black);

    //Number of rounds
    m_roundNum.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_roundNum.setString(roundButton);
    m_roundNum.setOrigin(m_roundNum.getLocalBounds().width / 2,
                         m_roundNum.getLocalBounds().height / 2);
    m_roundNum.setPosition(m_context->m_window->getSize().x / 2 + 27.f,
                           m_context->m_window->getSize().y / 2 + 75.f);
    m_roundNum.setCharacterSize(20);
    m_roundNum.setFillColor(sf::Color::Black);

    //Layout
    m_layout.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_layout.setString(layout);
    m_layout.setOrigin(m_layout.getLocalBounds().width / 2,
                       m_layout.getLocalBounds().height / 2);
    m_layout.setPosition(m_context->m_window->getSize().x / 2 + 27.f,
                           m_context->m_window->getSize().y / 2 + 95.f);
    m_layout.setCharacterSize(20);
    m_layout.setFillColor(sf::Color::Black);

    // Exit to main menu Button
    m_exitInMainMenu.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitInMainMenu.setString("Exit in main menu");
    m_exitInMainMenu.setOrigin(m_exitInMainMenu.getLocalBounds().width / 2,
                                     m_exitInMainMenu.getLocalBounds().height / 2);
    m_exitInMainMenu.setPosition(m_context->m_window->getSize().x / 2 + 50.f,
                               m_context->m_window->getSize().y / 2 + 140.f);
    m_exitInMainMenu.setCharacterSize(20);
    m_exitInMainMenu.setFillColor(sf::Color::Black);


}

void Settings::ProcessInput(){
    sf::Event event;
    while (m_context->m_window->pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_context->m_window->close();
        } else if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Up: {
                    if (m_indexButtonMenu1 > 0) --m_indexButtonMenu1;
                    if (m_indexButtonMenu1 == 0) {
                        m_isWithWallsSelected = true;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 1) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = true;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 2) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = true;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 3) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = true;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 4) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = true;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 5) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down: {
                    if (m_indexButtonMenu1 < m_countButtonMenu1 - 1) ++m_indexButtonMenu1;

                    if (m_indexButtonMenu1 == 0) {
                        m_isWithWallsSelected = true;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 1) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = true;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 2) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = true;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 3) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = true;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 4) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = true;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 5) {
                        m_isWithWallsSelected = false;
                        m_isColorSelected = false;
                        m_isBotNumSelected = false;
                        m_isRoundNumSelected = false;
                        m_isLayoutSelected = false;
                        m_exitInMainMenuButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Return: {
                    m_isWithWallsPressed = false;
                    m_isColorPressed = false;
                    m_isBotNumPressed = false;
                    m_isRoundNumPressed = false;
                    m_isLayoutPressed = false;
                    m_exitInMainMenuButtonPressed = false;
                    if (m_isWithWallsSelected) {
                        m_isWithWallsPressed = true;
                        m_isColorPressed = false;
                        m_isBotNumPressed = false;
                        m_isRoundNumPressed = false;
                        m_isLayoutPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_isColorSelected) {
                        m_isWithWallsPressed = false;
                        m_isColorPressed = true;
                        m_isBotNumPressed = false;
                        m_isRoundNumPressed = false;
                        m_isLayoutPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_isBotNumSelected) {
                        m_isWithWallsPressed = false;
                        m_isColorPressed = false;
                        m_isBotNumPressed = true;
                        m_isRoundNumPressed = false;
                        m_isLayoutPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_isRoundNumSelected) {
                        m_isWithWallsPressed = false;
                        m_isColorPressed = false;
                        m_isBotNumPressed = false;
                        m_isRoundNumPressed = true;
                        m_isLayoutPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_isLayoutSelected) {
                        m_isWithWallsPressed = false;
                        m_isColorPressed = false;
                        m_isBotNumPressed = false;
                        m_isRoundNumPressed = false;
                        m_isLayoutPressed = true;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_exitInMainMenuButtonSelected) {
                        m_isWithWallsPressed = false;
                        m_isColorPressed = false;
                        m_isBotNumPressed = false;
                        m_isRoundNumPressed = false;
                        m_isLayoutPressed = false;
                        m_exitInMainMenuButtonPressed = true;
                    }

                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

}



void Settings::Update(sf::Time deltaTime){
    if(m_isWithWallsSelected)
    {
        m_withWalls.setFillColor(sf::Color::Blue);
        m_color.setFillColor(sf::Color::Black);
        m_botNum.setFillColor(sf::Color::Black);
        m_roundNum.setFillColor(sf::Color::Black);
        m_layout.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if (m_isColorSelected)
    {
        m_withWalls.setFillColor(sf::Color::Black);
        m_color.setFillColor(sf::Color::Blue);
        m_botNum.setFillColor(sf::Color::Black);
        m_roundNum.setFillColor(sf::Color::Black);
        m_layout.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if (m_isBotNumSelected){
        m_withWalls.setFillColor(sf::Color::Black);
        m_color.setFillColor(sf::Color::Black);
        m_botNum.setFillColor(sf::Color::Blue);
        m_roundNum.setFillColor(sf::Color::Black);
        m_layout.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if (m_isRoundNumSelected){
        m_withWalls.setFillColor(sf::Color::Black);
        m_color.setFillColor(sf::Color::Black);
        m_botNum.setFillColor(sf::Color::Black);
        m_roundNum.setFillColor(sf::Color::Blue);
        m_layout.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if (m_isLayoutSelected){
        m_withWalls.setFillColor(sf::Color::Black);
        m_color.setFillColor(sf::Color::Black);
        m_botNum.setFillColor(sf::Color::Black);
        m_roundNum.setFillColor(sf::Color::Black);
        m_layout.setFillColor(sf::Color::Blue);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else{
        m_withWalls.setFillColor(sf::Color::Black);
        m_color.setFillColor(sf::Color::Black);
        m_botNum.setFillColor(sf::Color::Black);
        m_roundNum.setFillColor(sf::Color::Black);
        m_layout.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Red);
    }

    //walls mode
    if(m_isWithWallsPressed && wallButton == "Without walls mode")
    {
        wallButton = "Classical mode(with walls)";
        //writeToFile(arr[0]);
        array[0] = 0;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);

    }
    else if(m_isWithWallsPressed && wallButton == "Classical mode(with walls)")
    {
        wallButton = "Without walls mode";
        array[0] = 1;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);

    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->setCurrent();
    }

    //color
    if(m_isColorPressed && color == "Blue"){
        color = "Red";
        array[1] = 1;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isColorPressed && color == "Red"){
        color = "Magenta";
        array[1] = 2;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isColorPressed && color == "Magenta"){
        color = "Blue";
        array[1] = 0;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }

    //bots number
    if(m_isBotNumPressed && botButton == "1 bot"){
        botButton = "2 bots";
        array[2] = 2;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isBotNumPressed && botButton == "2 bots"){
        botButton = "1 bot";
        array[2] = 1;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }

    //rounds number
    if(m_isRoundNumPressed && roundButton == "1 round"){
        roundButton = "3 rounds";
        array[3] = 3;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isRoundNumPressed && roundButton == "3 rounds"){
        roundButton = "5 rounds";
        array[3] = 5;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isRoundNumPressed && roundButton == "5 rounds"){
        roundButton = "1 round";
        array[3] = 1;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }

    //layout
    if(m_isLayoutPressed && layout == "Arrows"){
        layout = "WASD";
        array[5] = 0;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if(m_isLayoutPressed && layout == "WASD"){
        layout = "Arrows";
        array[5] = 1;
        m_context->m_works->add(std::make_unique<Settings>(m_context), true);
    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }

    writeToFile(array, "../data/settings.json");
}





void Settings::Draw() {

    m_context->m_window->clear();

    m_context->m_window->draw(m_backGround);

    m_context->m_window->draw(m_settingsTitle);

    m_context->m_window->draw(m_withWalls);

    m_context->m_window->draw(m_color);

    m_context->m_window->draw(m_botNum);

    m_context->m_window->draw(m_roundNum);

    m_context->m_window->draw(m_layout);

    m_context->m_window->draw(m_exitInMainMenu);

    m_context->m_window->display();

}
