#include "../h/sceneWork.h"

SceneWork::SceneWork() : m_add(false), m_replace(false), m_remove(false)
{
}

SceneWork::~SceneWork()
{
}

void SceneWork::add(std::unique_ptr<Scene> toAdd, bool replace)
{
    m_add = true;
    m_newScene = std::move(toAdd);

    m_replace = replace;
}

void SceneWork::setCurrent()
{
    m_remove = true;
}

void SceneWork::processSceneChange()
{
    if(m_remove && (!m_sceneStack.empty()))
    {
        m_sceneStack.pop();

        if(!m_sceneStack.empty())
        {
            m_sceneStack.top()->Start();
        }

        m_remove = false;
    }

    if(m_add)
    {
        if(m_replace && (!m_sceneStack.empty()))
        {
            m_sceneStack.pop();
            m_replace = false;
        }

        if(!m_sceneStack.empty())
        {
            m_sceneStack.top()->Pause();
        }

        m_sceneStack.push(std::move(m_newScene));
        m_sceneStack.top()->Init();
        m_sceneStack.top()->Start();
        m_add = false;
    }
}

std::unique_ptr<Scene> &SceneWork::getCurrent()
{
    return m_sceneStack.top();
}