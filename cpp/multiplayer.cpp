#include "../h/multiplayer.h"
#include "../h/gameOverMulti.h"
#include "../h/puaseGameMulti.h"
#include "../h/score.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>


#include <list>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;


Multiplayer::Multiplayer(std::shared_ptr<Context>& context)
	: m_context(context)
{
    srand(time(nullptr));
}
Multiplayer::~Multiplayer()
{
}

void Multiplayer::Init() {

}

void Multiplayer::ProcessInput() {
	sf::Clock clock;

    numRound.setFont(m_context->m_assets->getFont(MAIN_FONT));
    numRound.setString(to_string(scoreArr[1] + scoreArr[2] + 1));
	python.snakeTexture.loadFromFile("../assets/textures/snake.png");
    python2.snakeTexture.loadFromFile("../assets/textures/snakered.png");
	wall.loadFromFile("../assets/textures/wall1.jpg");
	foodTexture.loadFromFile("../assets/textures/food.png");
    m_context->m_assets->addTexture(BACK, "../assets/textures/grasss.jpg");
    background.setTexture(m_context->m_assets->getTexture(BACK));

    sf::Event event;
	while (m_context->m_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_context->m_window->close();
		}

		//ypravlenie ydava
		else if (event.type == sf::Event::KeyPressed)
		{
			switch (event.key.code)
			{
			case sf::Keyboard::Up:
				snake_direction = 0;
				break;
			case sf::Keyboard::Down:
				snake_direction = 2;
				break;
			case sf::Keyboard::Left:
				snake_direction = 3;
				break;
			case sf::Keyboard::Right:
				snake_direction = 1;
				break;
			case sf::Keyboard::S:
				snake_direction2 = 2;
				break;
			case sf::Keyboard::A:
				snake_direction2 = 3;
				break;
			case sf::Keyboard::D:
				snake_direction2 = 1;
				break;
			case sf::Keyboard::W:
				snake_direction2 = 0;
				break;
			case sf::Keyboard::Escape:
				m_context->m_works->add(std::make_unique<PauseGameMulti>(m_context));
				break;
				//}
			default:
				break;
			}
		}
	}
	while (clock.getElapsedTime() < sf::milliseconds(100)) {}
int _x, _y;
	m_context->m_window->clear();
    m_context->m_window->draw(background);
    python2.Move(snake_direction);
	python.Move(snake_direction2);

	bool newFoodPosFound = false;
	if (python2.ydav.front().x == nfoodX && python2.ydav.front().y == nfoodY) {
		python2.Grow(nfoodX, nfoodY, newFoodPosFound);
	}

	if (python.ydav.front().x == nfoodX && python.ydav.front().y == nfoodY) {
		python.Grow(nfoodX, nfoodY, newFoodPosFound);
	}

	python.isIntersectsSnake(python2.ydav);

	python2.isIntersectsSnake(python.ydav);

    //if(/*arr[0] == 0*/) {
        for (auto index = 600, jindex = 0; index != -15; index -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 600, jindex = 315; index != -15; index -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 0, jindex = 15; jindex != 330; jindex += 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});
        for (auto index = 585, jindex = 315; jindex != 0; jindex -= 15)
            border_wall.push_back(std::list<sSSegment>{{index, jindex}});

        for (auto index = 0; index < border_wall.size(); ++index) {
            for (auto border: border_wall[index]) {
                sf::Sprite part;
                part.setPosition(border.x, border.y);
                part.setTexture(wall);
                m_context->m_window->draw(part);
            }

        }
        python.WallMode();
        python2.WallMode();
    //} else {
        python.TeleportateThrowWall();
        python2.TeleportateThrowWall();
  //  }
	for (auto s2 : python2.ydav)
	{
		sf::Sprite part;
		part.setPosition(s2.x, s2.y);
		part.setTexture(python2.snakeTexture);
		m_context->m_window->draw(part);
	}

	for (auto s1 : python.ydav)
	{
		sf::Sprite part;
		part.setPosition(s1.x, s1.y);
		part.setTexture(python.snakeTexture);
		m_context->m_window->draw(part);
	}

	sf::Sprite food_shape;
	food_shape.setPosition(nfoodX, nfoodY);
	food_shape.setTexture(foodTexture);
    m_context->m_window->draw(numRound);
	m_context->m_window->draw(food_shape);
	python.isIntersectsSelf();
	python2.isIntersectsSelf();

	m_context->m_window->display();

    if (python.deadFlag && python2.deadFlag) {
        python.deadFlag = false;
        python2.deadFlag = false;
            scoreArr[0] = 4;
            writeToFile(scoreArr, "../data/score.json");
            m_context->m_works->add(std::make_unique<Score>(m_context), true);

    }

    if (python.deadFlag) {
        python.deadFlag = false;
        if (scoreArr[2] == arr[3] / 2  || scoreArr[1] == arr[3] / 2) {
            defoult("../data/score.json");
            m_context->m_works->add(std::make_unique<GameOverMulti>(m_context), true);
        } else {
            scoreArr[0] = 2;
            scoreArr[2] += 1;
            scoreArr[3] = 0;
            writeToFile(scoreArr, "../data/score.json");
            m_context->m_works->add(std::make_unique<Score>(m_context), true);
        }
    }

    if (python2.deadFlag) {
        python2.deadFlag = false;
        if (scoreArr[2] == arr[3] / 2  || scoreArr[1] == arr[3] / 2 ) {
            defoult("../data/score.json");
            m_context->m_works->add(std::make_unique<GameOverMulti>(m_context), true);
        } else {
            scoreArr[0] = 1;
            scoreArr[1] += 1;
            scoreArr[3] = 0;
            writeToFile(scoreArr, "../data/score.json");
            m_context->m_works->add(std::make_unique<Score>(m_context), true);
        }
    }
}

void Multiplayer::Update(sf::Time deltaTime) {
}



void Multiplayer::Draw() {

}

void Multiplayer::Pause() {

}

void Multiplayer::Start() {

}

