#include "../h/score.h"
#include "../h/mainMenu.h"
#include "../h/gameWithBots.h"
#include "../h/multiplayer.h"

#include <SFML/Window/Event.hpp>

Score::Score(std::shared_ptr<Context>& context)
        : m_context(context), m_isContinueButtonSelected(true),
          m_isContinueButtonPressed(false), m_isExitButtonSelected(false),
          m_isExitButtonPressed(false), m_isExitInMainMenuButtonSelected(false),
          m_isExitInMainMenuButtonPressed(false), m_indexButtonMenu(0), m_countButtonMenu(3)
{
}
Score::~Score()
{
}

void Score::Init()
{
    m_context->m_assets->addTexture(BACK, "../assets/textures/backSet.jpg");
    m_backGO.setTexture(m_context->m_assets->getTexture(BACK));

    // Title
    m_gameOverTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_gameOverTitle.setString("Score:");
    m_gameOverTitle.setFillColor(sf::Color::Red);

    m_gameOverTitle.setOrigin(m_gameOverTitle.getLocalBounds().width / 2,
                              m_gameOverTitle.getLocalBounds().height / 2);
    m_gameOverTitle.setPosition(m_context->m_window->getSize().x / 2 - 210.f,
                                m_context->m_window->getSize().y / 2 - 150.f);
    m_gameOverTitle.setCharacterSize(60);


    // Play Button
    m_continueButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_continueButton.setString("Continue");
    m_continueButton.setOrigin(m_continueButton.getLocalBounds().width / 2,
                            m_continueButton.getLocalBounds().height / 2);
    m_continueButton.setPosition(m_context->m_window->getSize().x / 2,
                              m_context->m_window->getSize().y / 2 - 25.f);
    m_continueButton.setCharacterSize(30);

    // Exit in main menu Button
    m_exitInMainMenuButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitInMainMenuButton.setString("Exit in main menu");
    m_exitInMainMenuButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                                     m_exitButton.getLocalBounds().height / 2);
    m_exitInMainMenuButton.setPosition(m_context->m_window->getSize().x / 2 - 135.f,
                                       m_context->m_window->getSize().y / 2);
    m_exitInMainMenuButton.setCharacterSize(30);

    // Exit Button
    m_exitButton.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitButton.setString("Exit");
    m_exitButton.setOrigin(m_exitButton.getLocalBounds().width / 2,
                           m_exitButton.getLocalBounds().height / 2);
    m_exitButton.setPosition(m_context->m_window->getSize().x / 2,
                             m_context->m_window->getSize().y / 2 + 50.f);
    m_exitButton.setCharacterSize(30);

    //Winner on the round
    m_winner.setFont((m_context->m_assets->getFont(MAIN_FONT)));
    bool draw = false;
    if (scoreArr[0] == 1) {
        m_winner.setString("player1");
    } else if(scoreArr[0] == 2) {
        m_winner.setString("player2");
    } else if(scoreArr[0] == 0) {
        m_winner.setString("bot");
    } else {
        m_winner.setString("Draw");
        draw = true;
    }

    m_winner.setOrigin(m_exitButton.getLocalBounds().width / 2,
                           m_exitButton.getLocalBounds().height / 2);
    m_winner.setPosition(m_context->m_window->getSize().x / 2 + 170.f,
                             m_context->m_window->getSize().y / 2 - 120.f);
    if(draw){
        m_winner.setFillColor(sf::Color::Green);
        draw = false;
    }
    if(scoreArr[0] == 1){
        m_winner.setFillColor(sf::Color::Blue);
    }
    if(scoreArr[0] == 2){
        m_winner.setFillColor(sf::Color::Red);
    }
    if(scoreArr[0] == 0){
        m_winner.setFillColor(sf::Color::Yellow);
    }
    if (scoreArr[3] == 1 && (scoreArr[0] == 1)) {
        if(scoreArr[4] == 0){
            m_winner.setFillColor(sf::Color::Blue);
        }
        if((scoreArr[4] == 1)){
            m_winner.setFillColor(sf::Color::Red);
        }
        if(scoreArr[4] == 2){
            m_winner.setFillColor(sf::Color::Magenta);
        }
    }


    m_winner.setCharacterSize(30);

    //count_round_win
    m_score.setFont((m_context->m_assets->getFont(MAIN_FONT)));
    if (scoreArr[0] == 1) {
        m_score.setString(std::to_string(scoreArr[1]));
    } else if((scoreArr[0] == 2) || ((scoreArr[0] == 0))) {
        m_score.setString(std::to_string(scoreArr[2]));
    }
    m_score.setOrigin(m_exitButton.getLocalBounds().width / 2,
                       m_exitButton.getLocalBounds().height / 2);
    m_score.setPosition(m_context->m_window->getSize().x / 2 - 0.f,
                         m_context->m_window->getSize().y / 2 - 145.f);
    m_score.setFillColor(sf::Color::Red);
    m_score.setCharacterSize(60);
}

void Score::ProcessInput()
{
    sf::Event event;
    while (m_context->m_window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_context->m_window->close();
        }
        else if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Up:
                {
                    if (m_indexButtonMenu > 0) --m_indexButtonMenu;
                    if (m_indexButtonMenu == 0)
                    {
                        m_isContinueButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isContinueButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isContinueButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down:
                {
                    if (m_indexButtonMenu < m_countButtonMenu - 1) ++m_indexButtonMenu;

                    if (m_indexButtonMenu == 0)
                    {
                        m_isContinueButtonSelected = true;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 1)
                    {
                        m_isContinueButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = true;
                        m_isExitButtonSelected = false;
                    }
                    if (m_indexButtonMenu == 2)
                    {
                        m_isContinueButtonSelected = false;
                        m_isExitInMainMenuButtonSelected = false;
                        m_isExitButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Return: {
                    m_isExitButtonPressed = false;
                    m_isContinueButtonPressed = false;
                    m_isExitInMainMenuButtonPressed = false;
                    if (m_isContinueButtonSelected) {
                        m_isContinueButtonPressed = true;
                    } else if(m_isExitInMainMenuButtonSelected) {
                        m_isExitInMainMenuButtonPressed = true;
                    } else {
                        m_isExitButtonPressed = true;
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

void Score::Update(sf::Time deltaTime)
{
    if(m_isContinueButtonSelected)
    {
        m_continueButton.setFillColor(sf::Color::Green);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_exitButton.setFillColor(sf::Color::White);
    }
    else if (m_isExitButtonSelected)
    {
        m_exitButton.setFillColor(sf::Color::Red);
        m_exitInMainMenuButton.setFillColor(sf::Color::White);
        m_continueButton.setFillColor(sf::Color::White);
    } else {
        m_exitButton.setFillColor(sf::Color::White);
        m_continueButton.setFillColor(sf::Color::White);
        m_exitInMainMenuButton.setFillColor(sf::Color::Magenta);
    }

    if(m_isContinueButtonPressed)
    {
        if (scoreArr[3] == 1) {
            m_context->m_works->add(std::make_unique<GameWithBots>(m_context), true);
        } else {
            m_context->m_works->add(std::make_unique<Multiplayer>(m_context), true);
        }
    }
    else if(m_isExitButtonPressed)
    {
        m_context->m_window->close();
    } else if (m_isExitInMainMenuButtonPressed)
    {
        m_context->m_window->setSize(sf::Vector2u(640,352));
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }
}

void Score::Draw()
{
    m_context->m_window->clear();
    m_context->m_window->draw(m_backGO);
    m_context->m_window->draw(m_winner);
    m_context->m_window->draw(m_score);
    m_context->m_window->draw(m_gameOverTitle);
    m_context->m_window->draw(m_continueButton);
    m_context->m_window->draw(m_exitInMainMenuButton);
    m_context->m_window->draw(m_exitButton);
    m_context->m_window->display();
}
