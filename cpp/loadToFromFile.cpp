#include "../h/loadToFromFile.h"

#define LEN 6 //длина массива на запись и чтение
#define SETTINGS "../data/settings.json" // файл с настройками

int writeToFile(int* parametersArray, const char* fileName){
    FILE *Write = fopen(fileName, "wb+");   //wb - флаг на запись бинарного файла
    int len = LEN;
    if (Write == NULL) { //если нельзя создать или открыть
        printf("Error");
        getchar();
        return 0;
    }
    //записываем число элементов в массиве (для его чтения далее)
    fwrite(&len, sizeof(int), 1, Write);
    /* &ArrOut - ссылка на массив, sizeof(int) - размер одного элемента,
     * len- кол-во элементов на запись, file - указатель на файл */
    fwrite(parametersArray, sizeof(int), len, Write); //запись самого массива
    fclose(Write);   //закрываем файл
    return 1;
}

int* readFromFile(const char* fileName){
    FILE *Read = fopen(fileName, "rt"); //rb - флаг на чтение бинарного файла

    if (Read == NULL) { //если нельзя открыть
        printf("Error");
        getchar();
        return nullptr;
    }
    int lenRead = LEN;    //здесь будет храниться число элементов для чтения в файле
    fread(&lenRead, sizeof(int), 1, Read);
    //выделяем память под массив для чтения
    int *ArrInput = (int*) malloc(lenRead*sizeof(int));
    //читаем данные
    fread(ArrInput, sizeof(int), lenRead, Read);
    fclose(Read);   //закрываем файл
    return ArrInput;
}

int defoult(const char* fileName)
{
    int def[LEN] = {0, 0, 0, 0, 0, 0};
    FILE *Write = fopen(fileName, "wb+");   //wb - флаг на запись бинарного файла
    int len = LEN;
    if (Write == NULL) { //если нельзя создать или открыть
        printf("Error");
        getchar();
        return 0;
    }
    //записываем число элементов в массиве (для его чтения далее)
    fwrite(&len, sizeof(int), 1, Write);
    /* &ArrOut - ссылка на массив, sizeof(int) - размер одного элемента,
     * len- кол-во элементов на запись, file - указатель на файл */
    fwrite(def, sizeof(int), len, Write); //запись самого массива
    fclose(Write);   //закрываем файл
    return 1;
}
