#include "../h/mainMenu.h"
#include "../h/gameMod.h"
#include "../h/multiplayer.h"
#include "../h/gameWithBots.h"
#include "../h/gamePlay.h"


#include <SFML/Window/Event.hpp>

GameMode::GameMode(std::shared_ptr<Context> &context)
        : m_context(context),  m_isPlaySelected(true), m_isPlayPressed(false), m_isMultySelected(false), m_isMultyPressed(false), m_isBotSelected(false), m_isBotPressed(false),
        m_exitInMainMenuButtonSelected(false), m_exitInMainMenuButtonPressed(false), m_indexButtonMenu1(0), m_countButtonMenu1(4)
{
}
// nnnnnnn,
GameMode::~GameMode()
{
}
// ssss
void GameMode::Init() {
    m_context->m_assets->addTexture(BACK, "../assets/textures/backSet.jpg");
    m_backGround.setTexture(m_context->m_assets->getTexture(BACK));

    // Title
    m_modeTitle.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_modeTitle.setString("Choose game mode");
    m_modeTitle.setFillColor(sf::Color::Red);

    m_modeTitle.setOrigin(m_modeTitle.getLocalBounds().width / 2,
                          m_modeTitle.getLocalBounds().height / 2);
    m_modeTitle.setPosition(m_context->m_window->getSize().x / 2 - 60.f,
                                m_context->m_window->getSize().y / 2 - 150.f);
    m_modeTitle.setCharacterSize(40);

    //play button
    m_play.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_play.setString("Play");
    m_play.setOrigin(m_play.getLocalBounds().width / 2,
                     m_play.getLocalBounds().height / 2);
    m_play.setPosition(m_context->m_window->getSize().x / 2 + 0.f,
                       m_context->m_window->getSize().y / 2 - 60.f);
    m_play.setCharacterSize(30);
    m_play.setFillColor(sf::Color::Green);


    //play with bots
    m_bot.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_bot.setString("With bots");
    m_bot.setOrigin(m_bot.getLocalBounds().width / 2,
                      m_bot.getLocalBounds().height / 2);
    m_bot.setPosition(m_context->m_window->getSize().x / 2 + 35.f,
                        m_context->m_window->getSize().y / 2 + 0.f);
    m_bot.setCharacterSize(20);
    m_bot.setFillColor(sf::Color::Black);



    //Multiplayer
    m_multy.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_multy.setString("Multiplayer");
    m_multy.setOrigin(m_multy.getLocalBounds().width / 2,
                      m_multy.getLocalBounds().height / 2);
    m_multy.setPosition(m_context->m_window->getSize().x / 2 + 45.f,
                           m_context->m_window->getSize().y / 2 + 40.f);
    m_multy.setCharacterSize(20);
    m_multy.setFillColor(sf::Color::Black);


    // Exit to main menu Button
    m_exitInMainMenu.setFont(m_context->m_assets->getFont(MAIN_FONT));
    m_exitInMainMenu.setString("Exit in main menu");
    m_exitInMainMenu.setOrigin(m_exitInMainMenu.getLocalBounds().width / 2,
                               m_exitInMainMenu.getLocalBounds().height / 2);
    m_exitInMainMenu.setPosition(m_context->m_window->getSize().x / 2 + 50.f,
                                 m_context->m_window->getSize().y / 2 + 140.f);
    m_exitInMainMenu.setCharacterSize(20);
    m_exitInMainMenu.setFillColor(sf::Color::Red);
}

void GameMode::ProcessInput() {
    sf::Event event;
    while (m_context->m_window->pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_context->m_window->close();
        } else if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Up: {
                    if (m_indexButtonMenu1 > 0) --m_indexButtonMenu1;

                    if (m_indexButtonMenu1 == 0) {
                        m_isPlaySelected = true;
                        m_isMultySelected = false;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 2) {
                        m_isPlaySelected = false;
                        m_isMultySelected = true;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 1) {
                        m_isPlaySelected = false;
                        m_isMultySelected = false;
                        m_isBotSelected = true;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 3) {
                        m_isPlaySelected = false;
                        m_isMultySelected = false;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = true;
                    }

                    break;
                }
                case sf::Keyboard::Down: {
                    if (m_indexButtonMenu1 < m_countButtonMenu1 - 1) ++m_indexButtonMenu1;

                    if (m_indexButtonMenu1 == 0) {
                        m_isPlaySelected = true;
                        m_isMultySelected = false;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 2) {
                        m_isPlaySelected = false;
                        m_isMultySelected = true;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 1) {
                        m_isPlaySelected = false;
                        m_isMultySelected = false;
                        m_isBotSelected = true;
                        m_exitInMainMenuButtonSelected = false;
                    }
                    if (m_indexButtonMenu1 == 3) {
                        m_isPlaySelected = false;
                        m_isMultySelected = false;
                        m_isBotSelected = false;
                        m_exitInMainMenuButtonSelected = true;
                    }
                    break;
                }
                case sf::Keyboard::Return: {
                    m_isPlayPressed = false;
                    m_isMultyPressed = false;
                    m_isBotPressed = false;
                    m_exitInMainMenuButtonPressed = false;
                    if (m_isPlaySelected) {
                        m_isPlayPressed = true;
                        m_isMultyPressed = false;
                        m_isBotPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_isMultySelected) {
                        m_isPlayPressed = false;
                        m_isMultyPressed = true;
                        m_isBotPressed = false;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if (m_isBotSelected) {
                        m_isPlayPressed = false;
                        m_isMultyPressed = false;
                        m_isBotPressed = true;
                        m_exitInMainMenuButtonPressed = false;
                    }
                    else if(m_exitInMainMenuButtonSelected) {
                        m_isPlayPressed = false;
                        m_isMultyPressed = false;
                        m_isBotPressed = false;
                        m_exitInMainMenuButtonPressed = true;
                    }

                    break;
                }
                default: {
                    break;
                }
            }
        }
    }
}

void GameMode::Update(sf::Time deltaTime) {
    if(m_isPlaySelected)
    {
        m_play.setFillColor(sf::Color::Green);
        m_multy.setFillColor(sf::Color::Black);
        m_bot.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if(m_isMultySelected)
    {
        m_play.setFillColor(sf::Color::Black);
        m_multy.setFillColor(sf::Color::Blue);
        m_bot.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if(m_isBotSelected)
    {
        m_play.setFillColor(sf::Color::Black);
        m_multy.setFillColor(sf::Color::Black);
        m_bot.setFillColor(sf::Color::Blue);
        m_exitInMainMenu.setFillColor(sf::Color::Black);
    }
    else if(m_exitInMainMenuButtonSelected)
    {
        m_play.setFillColor(sf::Color::Black);
        m_multy.setFillColor(sf::Color::Black);
        m_bot.setFillColor(sf::Color::Black);
        m_exitInMainMenu.setFillColor(sf::Color::Red);
    }

    if(m_isPlayPressed){
        m_context->m_works->add(std::make_unique<GamePlay>(m_context), true);
    }
    else if (m_exitInMainMenuButtonPressed)
    {
        m_context->m_works->add(std::make_unique<MainMenu>(m_context), true);
    }
    else if(m_isMultyPressed){
        //flag for multiplayer
        m_context->m_works->add(std::make_unique<Multiplayer>(m_context), true);
    }
    else if (m_isBotPressed)
    {
        m_context->m_works->add(std::make_unique<GameWithBots>(m_context), true);
    }


}

void GameMode::Draw() {

    m_context->m_window->clear();

    m_context->m_window->draw(m_backGround);

    m_context->m_window->draw(m_modeTitle);

    m_context->m_window->draw(m_play);

    m_context->m_window->draw(m_bot);

    m_context->m_window->draw(m_multy);

    m_context->m_window->draw(m_exitInMainMenu);

    m_context->m_window->display();

}
