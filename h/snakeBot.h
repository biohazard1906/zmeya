#pragma once
#include <list>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "map.h"


class SnakeBot : public sf::Drawable {
private:
    int snake_size = 20;
    int dir;
    int _x, _y;
public:
    bool deadFlagBot;
    std::list<sSSegment> snakeBot;
    sf::Texture snakeBotTexture;


    SnakeBot(int x, int y);
    ~SnakeBot();

    void Init(const sf::Texture& texture);
    void Move();
    void isIntersectsSnake(std::list<sSSegment> snake, int &count_dead);
    void Mind(int foodX, int foodY);
    void Grow(int &nfoodX , int &nfoodY, bool &newFoodPosFound);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};
