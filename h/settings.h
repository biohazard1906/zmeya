#pragma once

#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "game.h"
#include "scene.h"

class Settings : public Scene
{
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_settingsTitle;
    sf::Text m_withWalls;
    sf::Text m_color;

    sf::Text m_botNum;
    sf::Text m_roundNum;

    sf::Text m_layout;

    sf::Text m_exitInMainMenu;
    sf::Sprite m_backGround;

    int m_indexButtonMenu1;
    int m_countButtonMenu1;

    bool m_isWithWallsSelected;
    bool m_isWithWallsPressed;

    bool m_isColorSelected;
    bool m_isColorPressed;

    bool m_isBotNumSelected;
    bool m_isBotNumPressed;

    bool m_isLayoutSelected;
    bool m_isLayoutPressed;

    bool m_isRoundNumSelected;
    bool m_isRoundNumPressed;

    bool m_exitInMainMenuButtonSelected;
    bool m_exitInMainMenuButtonPressed;

public:




    Settings(std::shared_ptr<Context>& context);
    ~Settings();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;



};