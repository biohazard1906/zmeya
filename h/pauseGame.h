#pragma once

#include <memory>

#include <SFML/Graphics/Text.hpp>

#include "scene.h"
#include "game.h"


class PauseGame : public Scene
{
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_pauseTitle;
    sf::Text m_restartButton;
    sf::Text m_exitButton;
    sf::Text m_exitInMainMenuButton;

    int m_indexButtonMenu2;
    int m_countButtonMenu2;

    bool m_isRestartButtonSelected;
    bool m_isRestartButtonPressed;

    bool m_isExitInMainMenuButtonSelected;
    bool m_isExitInMainMenuButtonPressed;

    bool m_isExitButtonSelected;
    bool m_isExitButtonPressed;

public:
    PauseGame(std::shared_ptr<Context> &context);
    ~PauseGame();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
};