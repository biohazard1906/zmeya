#pragma once

#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "game.h"
#include "scene.h"

class Score : public Scene
{
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_gameOverTitle;
    sf::Text m_continueButton;
    sf::Text m_exitInMainMenuButton;
    sf::Text m_exitButton;
    sf::Text m_winner;
    sf::Text m_score;
    sf::Sprite m_backGO;
    int* scoreArr = readFromFile("../data/score.json");
    //int* arr = readFromFile("..data/settings.json");

    int m_indexButtonMenu;
    int m_countButtonMenu;

    bool m_isContinueButtonSelected;
    bool m_isContinueButtonPressed;

    bool m_isExitInMainMenuButtonSelected;
    bool m_isExitInMainMenuButtonPressed;

    bool m_isExitButtonSelected;
    bool m_isExitButtonPressed;

public:
    Score(std::shared_ptr<Context>& context);
    ~Score();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
};
