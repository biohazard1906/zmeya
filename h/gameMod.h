#pragma once

#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "game.h"
#include "scene.h"
#include "loadToFromFile.h"

class GameMode : public Scene
{
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_modeTitle;
    sf::Text m_multy;
    sf::Text m_play;
    sf::Text m_bot;
    sf::Text m_exitInMainMenu;
    sf::Sprite m_backGround;

    int m_indexButtonMenu1;
    int m_countButtonMenu1;

    bool m_isPlaySelected;
    bool m_isPlayPressed;

    bool m_isMultySelected;
    bool m_isMultyPressed;

    bool m_isBotSelected;
    bool m_isBotPressed;

    bool m_exitInMainMenuButtonSelected;
    bool m_exitInMainMenuButtonPressed;

public:




    GameMode(std::shared_ptr<Context>& context);
    ~GameMode();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;



};