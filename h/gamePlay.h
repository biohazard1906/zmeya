#pragma once

#include <memory>
#include <array>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


#include "scene.h"
#include "game.h"
#include "map.h"
#include "snake.h"



class GamePlay : public Scene {
private:
    std::shared_ptr<Context> m_context;
    Snake python{300,100};
    int* arr = readFromFile("../data/settings.json");

    const sf::Color BG_color = sf::Color(170, 215, 81);
    int ScorePlayer1 = 0;
    int snake_direction = 3;
    int nfoodX = 100;
    int nfoodY = 200;
    sf::Texture wall;
    sf::Texture foodTexture;
    sf::Sprite background;
    std::vector<std::list<sSSegment>> border_wall;
public:
    GamePlay(std::shared_ptr<Context>& context);
    ~GamePlay();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
    void Pause() override;
    void Start() override;
};