#pragma once

#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "game.h"
#include "scene.h"

class GameOverMulti : public Scene
{
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_gameOverTitle;
    sf::Text m_retryButton;
    sf::Text m_exitInMainMenuButton;
    sf::Text m_exitButton;
    sf::Sprite m_backGO;

    int m_indexButtonMenu;
    int m_countButtonMenu;

    bool m_isRetryButtonSelected;
    bool m_isRetryButtonPressed;

    bool m_isExitInMainMenuButtonSelected;
    bool m_isExitInMainMenuButtonPressed;

    bool m_isExitButtonSelected;
    bool m_isExitButtonPressed;

public:
    GameOverMulti(std::shared_ptr<Context>& context);
    ~GameOverMulti();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
};