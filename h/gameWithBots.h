#pragma once

#include <memory>
#include <array>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


#include "scene.h"
#include "game.h"
#include "map.h"
#include "snake.h"
#include "snakeBot.h"



class GameWithBots : public Scene {
private:
    std::shared_ptr<Context> m_context;
    Snake python{300,100};
    SnakeBot bot{300, 200};
    SnakeBot bot2{100, 260};
    int* arr = readFromFile("../data/settings.json");
    int* scoreArr = readFromFile("../data/score.json");
    int deadBots;
    const sf::Color BG_color = sf::Color(170, 215, 81);
    int snake_direction = 3;

    sf::Text numRound;
    sf::Texture wall;
    sf::Texture foodTexture;
    sf::Sprite background;
    std::vector<std::list<sSSegment>> border_wall;
public:
    GameWithBots(std::shared_ptr<Context>& context);
    ~GameWithBots();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
    void Pause() override;
    void Start() override;
};