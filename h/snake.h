#pragma once
#include <list>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "map.h"




class Snake : public sf::Drawable {
private:
    int snake_size = 20;
public:
    bool deadFlag;
    sf::Texture snakeTexture;
    std::list<sSSegment> ydav;
    int _x, _y;


    Snake(int x, int y);
    ~Snake();
    void WallMode();
    void TeleportateThrowWall();
    void Init(const sf::Texture& texture);
    void Move(int dir);
    void isIntersectsSnake(std::list<sSSegment> snake);
    void isIntersectsSelf();
    void Grow(int& nfoodX, int& nfoodY, bool& newFoodPosFound);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};
