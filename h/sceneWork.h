#pragma once
#include <stack>
#include <memory>
#include "scene.h"

class SceneWork {

private:
    std::stack<std::unique_ptr<Scene>> m_sceneStack;
    std::unique_ptr<Scene> m_newScene;

    bool m_add;
    bool m_replace;
    bool m_remove;

public:
    SceneWork();
    ~SceneWork();

    void add(std::unique_ptr<Scene> toAdd, bool replace = false);
    void setCurrent();
    void processSceneChange();
    std::unique_ptr<Scene>& getCurrent();
};