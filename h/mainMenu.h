#pragma once

#include <memory>


#include "SFML/Graphics/Sprite.hpp"
#include <SFML/Graphics/Text.hpp>

#include "scene.h"
#include "game.h"

class MainMenu : public Scene {
private:
    std::shared_ptr<Context> m_context;
    sf::Text m_gameTitle;
    sf::Text m_playButton;
    sf::Text m_settingButton;
    sf::Text m_exitButton;
    sf::Sprite m_background;

    int m_indexButtonMenu;
    int m_countButtonMenu;

    bool m_isPlayButtonSelected;
    bool m_isPlayButtonPressed;

    bool m_isSettingButtonSelected;
    bool m_isSettingButtonPressed;

    bool m_isExitButtonSelected;
    bool m_isExitButtonPressed;

public:
    MainMenu(std::shared_ptr<Context> &context);

    ~MainMenu();

    void Init() override;

    void ProcessInput() override;

    void Update(sf::Time deltaTime) override;

    void Draw() override;
};