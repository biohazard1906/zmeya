#pragma once

#include <memory>
#include <array>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


#include "scene.h"
#include "game.h"
#include "map.h"
#include "snake.h"



class Multiplayer : public Scene {
private:
    std::shared_ptr<Context> m_context;
    Snake python{300,100};
    Snake python2{300,260 };
    int* arr = readFromFile("../data/settings.json");
    int* scoreArr = readFromFile("../data/score.json");
public:

    const sf::Color BG_color = sf::Color(170, 215, 81);
    int player1 = 0;
    int player2 = 0;
    int snake_direction = 3, snake_direction2 = 3;
    int nfoodX = 100;
    int nfoodY = 200;
    sf::Texture wall;
    sf::Texture foodTexture;
    sf::Sprite background;
    std::vector<std::list<sSSegment>> border_wall;
    sf::Text numRound;

    Multiplayer(std::shared_ptr<Context>& context);
    ~Multiplayer();

    void Init() override;
    void ProcessInput() override;
    void Update(sf::Time deltaTime) override;
    void Draw() override;
    void Pause() override;
    void Start() override;
};