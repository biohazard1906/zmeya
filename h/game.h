#pragma once

#include <memory>

#include <SFML/Graphics/RenderWindow.hpp>

#include "sceneAsset.h"
#include "sceneWork.h"
#include "loadToFromFile.h"

enum AssetID
{
    MAIN_FONT = 0,
    GRASS,
    FOOD,
    WALL,
    SNAKE,
    BACK
};

struct Context
{
    std::unique_ptr<SceneAsset> m_assets;
    std::unique_ptr<SceneWork> m_works;
    std::unique_ptr<sf::RenderWindow> m_window;

    Context()
    {
        m_assets = std::make_unique<SceneAsset>();
        m_works = std::make_unique<SceneWork>();
        m_window = std::make_unique<sf::RenderWindow>();
    }
};

class Game
{
private:
    std::shared_ptr<Context> m_context;
    const sf::Time TIME_PER_FRAME = sf::seconds(1.f/60.f);


public:
    Game();
    ~Game();

    void Run();
};